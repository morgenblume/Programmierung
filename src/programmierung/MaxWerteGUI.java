package programmierung;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.List;
import java.awt.BorderLayout;
import java.awt.Choice;
import javax.swing.JLabel;
import javax.swing.JToolBar;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MaxWerteGUI {

	private JFrame frmMaxwertegui;
	private JTextField aField;
	private JTextField bField;
	private JTextField maxField;
	private JTextField xField;
	private JTextField cField;
	private JTextField dField;
	private JTextField ergField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MaxWerteGUI window = new MaxWerteGUI();
					window.frmMaxwertegui.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MaxWerteGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMaxwertegui = new JFrame();
		frmMaxwertegui.setTitle("MaxWerteGUI");
		frmMaxwertegui.setBounds(100, 100, 450, 300);
		frmMaxwertegui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 74, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 32, 0, 0, 0, 0, 23, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		frmMaxwertegui.getContentPane().setLayout(gridBagLayout);
		
		JLabel lblFxAx = new JLabel("f(x) = a*x^3 + b*x^2 + c*x +d");
		GridBagConstraints gbc_lblFxAx = new GridBagConstraints();
		gbc_lblFxAx.insets = new Insets(0, 0, 5, 5);
		gbc_lblFxAx.gridx = 3;
		gbc_lblFxAx.gridy = 1;
		frmMaxwertegui.getContentPane().add(lblFxAx, gbc_lblFxAx);
		
		JLabel lblA = new JLabel("a =");
		GridBagConstraints gbc_lblA = new GridBagConstraints();
		gbc_lblA.anchor = GridBagConstraints.EAST;
		gbc_lblA.insets = new Insets(0, 0, 5, 5);
		gbc_lblA.gridx = 2;
		gbc_lblA.gridy = 2;
		frmMaxwertegui.getContentPane().add(lblA, gbc_lblA);
		
		aField = new JTextField();
		GridBagConstraints gbc_aField = new GridBagConstraints();
		gbc_aField.anchor = GridBagConstraints.WEST;
		gbc_aField.insets = new Insets(0, 0, 5, 5);
		gbc_aField.gridx = 3;
		gbc_aField.gridy = 2;
		frmMaxwertegui.getContentPane().add(aField, gbc_aField);
		aField.setColumns(10);
		
		JLabel lblB = new JLabel("b = ");
		GridBagConstraints gbc_lblB = new GridBagConstraints();
		gbc_lblB.anchor = GridBagConstraints.EAST;
		gbc_lblB.insets = new Insets(0, 0, 5, 5);
		gbc_lblB.gridx = 2;
		gbc_lblB.gridy = 3;
		frmMaxwertegui.getContentPane().add(lblB, gbc_lblB);
		
		bField = new JTextField();
		GridBagConstraints gbc_bField = new GridBagConstraints();
		gbc_bField.anchor = GridBagConstraints.WEST;
		gbc_bField.insets = new Insets(0, 0, 5, 5);
		gbc_bField.gridx = 3;
		gbc_bField.gridy = 3;
		frmMaxwertegui.getContentPane().add(bField, gbc_bField);
		bField.setColumns(10);
		
		JLabel lblC = new JLabel("c = ");
		GridBagConstraints gbc_lblC = new GridBagConstraints();
		gbc_lblC.anchor = GridBagConstraints.EAST;
		gbc_lblC.insets = new Insets(0, 0, 5, 5);
		gbc_lblC.gridx = 2;
		gbc_lblC.gridy = 4;
		frmMaxwertegui.getContentPane().add(lblC, gbc_lblC);
		
		cField = new JTextField();
		GridBagConstraints gbc_cField = new GridBagConstraints();
		gbc_cField.anchor = GridBagConstraints.WEST;
		gbc_cField.insets = new Insets(0, 0, 5, 5);
		gbc_cField.gridx = 3;
		gbc_cField.gridy = 4;
		frmMaxwertegui.getContentPane().add(cField, gbc_cField);
		cField.setColumns(10);
		
		JLabel lblD = new JLabel("d =");
		GridBagConstraints gbc_lblD = new GridBagConstraints();
		gbc_lblD.anchor = GridBagConstraints.EAST;
		gbc_lblD.insets = new Insets(0, 0, 5, 5);
		gbc_lblD.gridx = 2;
		gbc_lblD.gridy = 5;
		frmMaxwertegui.getContentPane().add(lblD, gbc_lblD);
		
		dField = new JTextField();
		GridBagConstraints gbc_dField = new GridBagConstraints();
		gbc_dField.anchor = GridBagConstraints.WEST;
		gbc_dField.insets = new Insets(0, 0, 5, 5);
		gbc_dField.gridx = 3;
		gbc_dField.gridy = 5;
		frmMaxwertegui.getContentPane().add(dField, gbc_dField);
		dField.setColumns(10);
		
		JButton btnRechnen = new JButton("Rechnen");
		btnRechnen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				double[] erg = MaxWerte.rechnen(parse2D(aField), parse2D(bField), parse2D(cField), 
						parse2D(dField));
				xField.setText(String.format("%.2f", erg[0]));
				maxField.setText(String.format("%.2f", erg[1]));
				ergField.setText("Keine L�sung");
				
				
			}

			private double parse2D(JTextField jtf) {
				// TODO Auto-generated method stub
				return Double.parseDouble(jtf.getText());
			}
		});
		
		frmMaxwertegui.getRootPane().setDefaultButton(btnRechnen);
		GridBagConstraints gbc_btnRechnen = new GridBagConstraints();
		gbc_btnRechnen.insets = new Insets(0, 0, 5, 5);
		gbc_btnRechnen.gridx = 3;
		gbc_btnRechnen.gridy = 6;
		frmMaxwertegui.getContentPane().add(btnRechnen, gbc_btnRechnen);
		
		JLabel lblBeiX = new JLabel("Bei x =");
		GridBagConstraints gbc_lblBeiX = new GridBagConstraints();
		gbc_lblBeiX.anchor = GridBagConstraints.EAST;
		gbc_lblBeiX.insets = new Insets(0, 0, 5, 5);
		gbc_lblBeiX.gridx = 2;
		gbc_lblBeiX.gridy = 7;
		frmMaxwertegui.getContentPane().add(lblBeiX, gbc_lblBeiX);
		
		xField = new JTextField();
		GridBagConstraints gbc_xField = new GridBagConstraints();
		gbc_xField.anchor = GridBagConstraints.WEST;
		gbc_xField.insets = new Insets(0, 0, 5, 5);
		gbc_xField.gridx = 3;
		gbc_xField.gridy = 7;
		frmMaxwertegui.getContentPane().add(xField, gbc_xField);
		xField.setColumns(10);
		
		JLabel lblFxMax = new JLabel("f(x) max =");
		GridBagConstraints gbc_lblFxMax = new GridBagConstraints();
		gbc_lblFxMax.anchor = GridBagConstraints.EAST;
		gbc_lblFxMax.insets = new Insets(0, 0, 5, 5);
		gbc_lblFxMax.gridx = 2;
		gbc_lblFxMax.gridy = 8;
		frmMaxwertegui.getContentPane().add(lblFxMax, gbc_lblFxMax);
		
		maxField = new JTextField();
		GridBagConstraints gbc_maxField = new GridBagConstraints();
		gbc_maxField.anchor = GridBagConstraints.WEST;
		gbc_maxField.insets = new Insets(0, 0, 5, 5);
		gbc_maxField.gridx = 3;
		gbc_maxField.gridy = 8;
		frmMaxwertegui.getContentPane().add(maxField, gbc_maxField);
		maxField.setColumns(10);
		
		ergField = new JTextField();
		GridBagConstraints gbc_ergField = new GridBagConstraints();
		gbc_ergField.insets = new Insets(0, 0, 0, 5);
		gbc_ergField.fill = GridBagConstraints.HORIZONTAL;
		gbc_ergField.gridx = 3;
		gbc_ergField.gridy = 9;
		frmMaxwertegui.getContentPane().add(ergField, gbc_ergField);
		ergField.setColumns(10);
	}

	public JFrame getFrame() {
		// TODO Auto-generated method stub
		return frmMaxwertegui;
	}

}
