package programmierung;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FunktionsTypAGUI {

	private JFrame frmFunktionstypA;
	private JTextField aField;
	private JTextField alphaField;
	private JTextField betaField;
	private JTextField p1Field;
	private JTextField p2Field;
	private JTextField rField;
	private JTextField v1Field;
	private JTextField v2Field;

	/**
	 * Create the application.
	 */
	public FunktionsTypAGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmFunktionstypA = new JFrame();
		frmFunktionstypA.setTitle("Funktionstyp A");
		frmFunktionstypA.setBounds(100, 100, 450, 300);
		frmFunktionstypA.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		frmFunktionstypA.getContentPane().setLayout(gridBagLayout);
		
		JLabel lblRAvalphavbeta = new JLabel("r = a * v1^alpha * v2^beta");
		GridBagConstraints gbc_lblRAvalphavbeta = new GridBagConstraints();
		gbc_lblRAvalphavbeta.insets = new Insets(0, 0, 5, 0);
		gbc_lblRAvalphavbeta.gridx = 2;
		gbc_lblRAvalphavbeta.gridy = 0;
		frmFunktionstypA.getContentPane().add(lblRAvalphavbeta, gbc_lblRAvalphavbeta);
		
		JLabel lblA = new JLabel("a =");
		GridBagConstraints gbc_lblA = new GridBagConstraints();
		gbc_lblA.anchor = GridBagConstraints.EAST;
		gbc_lblA.insets = new Insets(0, 0, 5, 5);
		gbc_lblA.gridx = 1;
		gbc_lblA.gridy = 1;
		frmFunktionstypA.getContentPane().add(lblA, gbc_lblA);
		
		aField = new JTextField();
		GridBagConstraints gbc_aField = new GridBagConstraints();
		gbc_aField.anchor = GridBagConstraints.WEST;
		gbc_aField.insets = new Insets(0, 0, 5, 0);
		gbc_aField.gridx = 2;
		gbc_aField.gridy = 1;
		frmFunktionstypA.getContentPane().add(aField, gbc_aField);
		aField.setColumns(10);
		
		JLabel lblAlpha = new JLabel("alpha =");
		GridBagConstraints gbc_lblAlpha = new GridBagConstraints();
		gbc_lblAlpha.insets = new Insets(0, 0, 5, 5);
		gbc_lblAlpha.anchor = GridBagConstraints.EAST;
		gbc_lblAlpha.gridx = 1;
		gbc_lblAlpha.gridy = 2;
		frmFunktionstypA.getContentPane().add(lblAlpha, gbc_lblAlpha);
		
		alphaField = new JTextField();
		GridBagConstraints gbc_alphaField = new GridBagConstraints();
		gbc_alphaField.anchor = GridBagConstraints.WEST;
		gbc_alphaField.insets = new Insets(0, 0, 5, 0);
		gbc_alphaField.gridx = 2;
		gbc_alphaField.gridy = 2;
		frmFunktionstypA.getContentPane().add(alphaField, gbc_alphaField);
		alphaField.setColumns(10);
		
		JLabel lblBeta = new JLabel("beta =");
		GridBagConstraints gbc_lblBeta = new GridBagConstraints();
		gbc_lblBeta.insets = new Insets(0, 0, 5, 5);
		gbc_lblBeta.anchor = GridBagConstraints.EAST;
		gbc_lblBeta.gridx = 1;
		gbc_lblBeta.gridy = 3;
		frmFunktionstypA.getContentPane().add(lblBeta, gbc_lblBeta);
		
		betaField = new JTextField();
		GridBagConstraints gbc_betaField = new GridBagConstraints();
		gbc_betaField.anchor = GridBagConstraints.WEST;
		gbc_betaField.insets = new Insets(0, 0, 5, 0);
		gbc_betaField.gridx = 2;
		gbc_betaField.gridy = 3;
		frmFunktionstypA.getContentPane().add(betaField, gbc_betaField);
		betaField.setColumns(10);
		
		JLabel lblP = new JLabel("p1 =");
		GridBagConstraints gbc_lblP = new GridBagConstraints();
		gbc_lblP.insets = new Insets(0, 0, 5, 5);
		gbc_lblP.anchor = GridBagConstraints.EAST;
		gbc_lblP.gridx = 1;
		gbc_lblP.gridy = 4;
		frmFunktionstypA.getContentPane().add(lblP, gbc_lblP);
		
		p1Field = new JTextField();
		GridBagConstraints gbc_p1Field = new GridBagConstraints();
		gbc_p1Field.anchor = GridBagConstraints.WEST;
		gbc_p1Field.insets = new Insets(0, 0, 5, 0);
		gbc_p1Field.gridx = 2;
		gbc_p1Field.gridy = 4;
		frmFunktionstypA.getContentPane().add(p1Field, gbc_p1Field);
		p1Field.setColumns(10);
		
		JLabel lblP_1 = new JLabel("p2 =");
		GridBagConstraints gbc_lblP_1 = new GridBagConstraints();
		gbc_lblP_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblP_1.anchor = GridBagConstraints.EAST;
		gbc_lblP_1.gridx = 1;
		gbc_lblP_1.gridy = 5;
		frmFunktionstypA.getContentPane().add(lblP_1, gbc_lblP_1);
		
		p2Field = new JTextField();
		GridBagConstraints gbc_p2Field = new GridBagConstraints();
		gbc_p2Field.anchor = GridBagConstraints.WEST;
		gbc_p2Field.insets = new Insets(0, 0, 5, 0);
		gbc_p2Field.gridx = 2;
		gbc_p2Field.gridy = 5;
		frmFunktionstypA.getContentPane().add(p2Field, gbc_p2Field);
		p2Field.setColumns(10);
		
		JLabel lblR = new JLabel("r =");
		GridBagConstraints gbc_lblR = new GridBagConstraints();
		gbc_lblR.insets = new Insets(0, 0, 5, 5);
		gbc_lblR.anchor = GridBagConstraints.EAST;
		gbc_lblR.gridx = 1;
		gbc_lblR.gridy = 6;
		frmFunktionstypA.getContentPane().add(lblR, gbc_lblR);
		
		rField = new JTextField();
		GridBagConstraints gbc_rField = new GridBagConstraints();
		gbc_rField.insets = new Insets(0, 0, 5, 0);
		gbc_rField.anchor = GridBagConstraints.WEST;
		gbc_rField.gridx = 2;
		gbc_rField.gridy = 6;
		frmFunktionstypA.getContentPane().add(rField, gbc_rField);
		rField.setColumns(10);
		
		JButton btnRechnen = new JButton("Rechnen");
		btnRechnen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				double[] erg = FunktionsTypA.rechnen(parse2D(aField), parse2D(alphaField), parse2D(betaField), 
						parse2D(rField), parse2D(p1Field), parse2D(p2Field));
				v1Field.setText(""+erg[0]);
				v2Field.setText(""+erg[1]);
			}
		});
		frmFunktionstypA.getRootPane().setDefaultButton(btnRechnen);
		
		GridBagConstraints gbc_btnRechnen = new GridBagConstraints();
		gbc_btnRechnen.insets = new Insets(0, 0, 5, 0);
		gbc_btnRechnen.gridx = 2;
		gbc_btnRechnen.gridy = 7;
		frmFunktionstypA.getContentPane().add(btnRechnen, gbc_btnRechnen);
		
		JLabel lblV = new JLabel("v1 =");
		GridBagConstraints gbc_lblV = new GridBagConstraints();
		gbc_lblV.anchor = GridBagConstraints.EAST;
		gbc_lblV.insets = new Insets(0, 0, 5, 5);
		gbc_lblV.gridx = 1;
		gbc_lblV.gridy = 8;
		frmFunktionstypA.getContentPane().add(lblV, gbc_lblV);
		
		v1Field = new JTextField();
		GridBagConstraints gbc_v1Field = new GridBagConstraints();
		gbc_v1Field.insets = new Insets(0, 0, 5, 0);
		gbc_v1Field.anchor = GridBagConstraints.WEST;
		gbc_v1Field.gridx = 2;
		gbc_v1Field.gridy = 8;
		frmFunktionstypA.getContentPane().add(v1Field, gbc_v1Field);
		v1Field.setColumns(10);
		
		JLabel lblV_1 = new JLabel("v2 =");
		GridBagConstraints gbc_lblV_1 = new GridBagConstraints();
		gbc_lblV_1.anchor = GridBagConstraints.EAST;
		gbc_lblV_1.insets = new Insets(0, 0, 0, 5);
		gbc_lblV_1.gridx = 1;
		gbc_lblV_1.gridy = 9;
		frmFunktionstypA.getContentPane().add(lblV_1, gbc_lblV_1);
		
		v2Field = new JTextField();
		GridBagConstraints gbc_v2Field = new GridBagConstraints();
		gbc_v2Field.anchor = GridBagConstraints.WEST;
		gbc_v2Field.gridx = 2;
		gbc_v2Field.gridy = 9;
		frmFunktionstypA.getContentPane().add(v2Field, gbc_v2Field);
		v2Field.setColumns(10);
	}
	
	public JFrame getFrame(){
		return frmFunktionstypA;
	}
	
	private double parse2D(JTextField jtf){
		return Double.parseDouble(jtf.getText());
	}

}
