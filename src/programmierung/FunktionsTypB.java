package programmierung;

public class FunktionsTypB {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
float a1 = 2f, b1 = -16f, c1 = 40f, a2 =2.5f, b2 =-28f, c2 = 80f, p1 = 3, p2 = 4, t = 8;
		
		float x = a1*p1 + a2*p2;
		float y = b1*p1 + b2*p2;
		float z = c1*p1 + c2*p2;
		
		float d = -y/(2*x);
		float max = (float)(x*Math.pow(d, 2) + y*d + z);
		float v1 = (float)((a1*Math.pow(d, 2) + b1*d + c1)*d*t);
		float v2 = (float)((a2*Math.pow(d, 2) + b2*d + c2)*d*t);
		float d1 = -b1/(2*a1);
		float t1 = (d*t)/d1;
		float erg = (float)((a1*Math.pow(d1, 2) + b1*d1 + c1)*d1*t1);
		
		
		
		System.out.println("r(d) = " + x +"*d^2 + (" + y + "*d) + " + z);
		System.out.printf("d(opt) = %.2f\n", d);
		System.out.printf("r(d)opt = %.2f\n", max);
		System.out.println("Mengeneinheiten des Faktors 1 werden t�glich verbraucht: " + v1);
		System.out.println("Mengeneinheiten des Faktors 2 werden t�glich verbraucht: " + v2);
		System.out.println("Mengeneinheiten des Faktors 1 werden bei gleicher t�glicher Produktionsmenge am Tag verbraucht: " + erg);

	}

}
